# Tutorial 1

If you haven't done already, create a [BitBucket](https://bitbucket.org/account/signup/) account and the sign in.

## Create Repository
After logging in to your BitBucket account, create a new repository called `CSCC01_tutorial_1`.

During the creation of the repo, when you are asked if you want to include a README.md select `Yes, with a tutorial (for beginners)`.

After creating the repo, read through the README.md file and complete the `Edit a file` and `Create a file` section in the README.md.

## Clone Repo
You will then clone this repo to your local drive through command line. First find the `clone` button in the root folder in `Source`. You will then copy and paste the `git clone ...` command into your preferred terminal.

This will clone your repo from BitBucket on to your local drive.

## Create new file
Now create a new branch called `student-shell-script` and checkout that branch.

Then create a shell script which will be called `student_name.sh`. Inside the file you will `echo` your name.

After that run `sh student_name.sh` in the command line and it should print out your name in the terminal.

Now `add` this file to the staging area in git, then `commit` it with a meaningful message and finally `push` it to on to your new branch.

After confirming that you have successfully pushed your script into the git repo, you will then merge master into the branch (resolve any conflicts if you have any) then merge the branch back into the master branch.

## Removing a file
Now create a new branch called `removing-contributors-file` and checkout that branch

You will now remove the `contributors.txt` file from your working directory and then update the branch with these changes through the command line.

After confirming that you have successfully removed the file from the git repo, you will then merge master into the branch (resolve any conflicts if you have any) then merge the branch back into the master branch.

